<?php
use R3C\Wordpress\Doctrine\DoctrineFactory;
use R3C\Wordpress\Router\Router;
use R3C\Wordpress\EventDispatcher\EventDispatcher;
use R3C\Wordpress\Twig\TwigFactory;
use R3C\Wordpress\LiveEditor\TwigExtensions\LiveEditorTwigExtension;
use R3C\Wordpress\LiveEditor\TwigExtensions\TwigNodeLiveEditor;

DoctrineFactory::addEntityPath('R3C\Wordpress\LiveEditor\Entities', __DIR__ . '/src/R3C/Wordpress/LiveEditor/Entities');

$router = new Router();
$router->addResource('/api/live-editor/page-element', 'R3C\Wordpress\LiveEditor\Controllers\PageElementController');
$router->checkAndRun();

TwigFactory::addExtension(new LiveEditorTwigExtension());

EventDispatcher::addListener(function() {
    add_action( 'wp_enqueue_scripts', 'r3cWordpressLiveEditorScripts' );
    add_filter('show_admin_bar', '__return_false');
});

function r3cWordpressLiveEditorScripts() {
    $url = str_replace('/wp', '/', get_site_url());
    $js = $url . 'wp-content/plugins/wordpress-live-editor/js/';
    $css = $url . 'wp-content/plugins/wordpress-live-editor/';

	if ( current_user_can('manage_options')) {
        wp_register_script( 'r3c-wordpress-live-editor', $js . 'code.min.js', array(), '1.0');
        wp_localize_script('r3c-wordpress-live-editor', 'siteInfo', ['baseUrl' => $url]);
        wp_enqueue_script('r3c-wordpress-live-editor');

        wp_enqueue_style('r3c-wordpress-live-editor', $css . 'style.css', array(), '1.0');

        //Carrega os scripts do Wordpress Admin para ter a caixa de upload de Media
        wp_enqueue_media();
	}
}

?>