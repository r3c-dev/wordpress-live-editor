## R3C Wordpress Live Editor

Live Editor para páginas Wordpress

# Forma de uso #

Nas páginas que deseja usar, insira algo como:


```
#!php
use R3C\Wordpress\LiveEditor\LiveEditor;

<div data-live-editor="true" data-live-editor-name="um-nome-unico">
    <?php
        LiveEditor::pageElement('um-nome-unico', function() {
    ?>

    CONTEÚDO PADRÃO

    <?php
        });
    ?>
</div>
```

# Integração com Twig #
Para realizar a chamada do live editor atraves do twig:

```
#!html
<dvi data-live-edito="true" data-live-editor-name="um-nome-unico">
	{% liveEditor 'um-nome-unico' %}
	CONTEÚDO PADRÃO
	{% endLiveEditor %}
</dvi>
```

IMPORTANTE: o parametro passado a tag twig liveEditor, deve ser o valor do atributo data-live-edito-name que deseja exibir
