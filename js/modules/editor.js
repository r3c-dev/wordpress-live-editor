/**
 * R3C Live Editor
 */

Modules.liveEditor = {

	start: function() {
		if (CKEDITOR != undefined) {
			CKEDITOR.plugins.add( 'wordpress-images', {
			    init: function(editor) {
			        var command = new CKEDITOR.command(editor, {
					    exec: function(editor) {
					        Modules.liveEditor.openUploadWindow();
					    }
					});

					editor.addCommand('wordpress-images', command);

			        editor.ui.addButton( 'WPImages', {
			            label: 'Inserir imagens do Wordpress',
			            command: 'wordpress-images',
			            toolbar: 'insert',
			            icon: siteInfo.baseUrl + 'wp-content/plugins/wordpress-live-editor/images/ckeditor-wordpress-images-toolbar.png'
			        });
			    }
			});

			CKEDITOR.config.extraPlugins = CKEDITOR.config.extraPlugins + ',wordpress-images';
			CKEDITOR.config.language = 'pt-br';
			CKEDITOR.config.floatSpaceDockedOffsetX = 0;
    		CKEDITOR.config.floatSpaceDockedOffsetY = 70;

			CKEDITOR.config.allowedContent = true;

			//add ckeditor
			if ($('body [data-live-editor="true"]').length > 0) {

				$('body').append(
					'<div id="live-editor-menu" class="ui blue huge launch right attached button"> \
  						<i class="icon list layout"></i> \
  						<span style="display: none;" class="text">Menu</span> \
      				</div>'
      			);

      			$('#live-editor-menu').click(function() {
      				$('#live-editor-sidebar').sidebar('toggle');
      			});

				$('body').append(
					'<div class="ui styled left sidebar" id="live-editor-sidebar" style="padding-top: 50px;"> \
						<h3 class="ui header">Ações</h3> \
						<div class="ui form"> \
							<div class="ui black fluid button" id="live-editor-save">Salvar</div> \
						</div> \
						<div class="ui vertical menu historico"> \
							<div class="header item"> \
								<i class="time icon"></i> \
								Histórico \
							</div> \
						</div> \
					</div>'
				);

				$('#live-editor-sidebar').sidebar();

				$.each($('body [data-live-editor="true"]'), function(index, element) {
					$(element).attr('contenteditable', true);
					var ck = CKEDITOR.inline($(element).get(0));

					ck.on('focus', function() {
						Modules.liveEditor.currentEditor = ck;
						$('#live-editor-sidebar').sidebar('show');

						var name = $(element).attr('data-live-editor-name');

						if (Modules.liveEditor.currentName != name) {
							Modules.liveEditor.currentName = name;
							Modules.liveEditor.getModifications(name);
						}
					});
				});

				$('#live-editor-save').click(function() {
					var items = {
						name: [],
						content: []
					};

					$.each(CKEDITOR.instances, function(key, item) { 
						if (item.checkDirty()) {
							var element = item.element.$;
							var name = $(element).attr('data-live-editor-name');
							var content = $(element).html();
							
							
							items.name.push(name);
							items.content.push(content);
						}
					});

					if (items.name.length > 0) {
						Modules.liveEditor.save(items);
					}

					return false;
				});
			}
		}
	},

	currentEditor: null,
	fileFrame: false,

	save: function(items) {
		items = $.param(items);

		var url = siteInfo.baseUrl + 'api/live-editor/page-element';

		$('#live-editor-save').addClass('loading');
		$('#live-editor-save .icon').remove();

		$.post(url, items, function(data) {
			$('#live-editor-save').removeClass('loading');
			$('#live-editor-save').prepend('<i class="checkmark icon"></i>');

			Modules.liveEditor.getModifications(Modules.liveEditor.currentName);
		}, 'json');
	},

	currentName: '',

	getModifications: function(name) {
		var url = siteInfo.baseUrl + 'api/live-editor/page-element';

		$('#live-editor-sidebar .historico a.item').remove();

		$('#live-editor-sidebar .historico').append('<a class="item" href="#">carregando...</a>');

		$.get(url, { name: name } , function(data) {
			$('#live-editor-sidebar .historico a.item').remove();

			$.each(data, function(index, item) {
				//A data deve vir no formato 2014-10-31T19:28:22+0000
				var data = new Date(item.created_at);

				var dia = data.getDate();
				var mes = data.getMonth() + 1;
				var ano = data.getFullYear();
				var hora = data.getHours();
				var minuto = data.getMinutes();

				if (dia < 10) {
					dia = '0' + dia;
				}

				if (mes < 10) {
					mes = '0' + mes;
				}

				if (hora < 10) {
					hora = '0' + hora;
				}

				if (minuto < 10) {
					minuto = '0' + minuto;
				}

				var dataString = dia + '/' + mes + '/' + ano + ' ' + hora + ':' + minuto;

				$('#live-editor-sidebar .historico').append('<a class="item" href="#" data-id="' + item.id + '" data-name="' + item.name + '"> <i class="ui icon unhide"></i>' + dataString + '</a>');
			});

			Modules.liveEditor.clickModifications();
		}, 'json');
	},

	clickModifications: function() {
		$('#live-editor-sidebar .historico a.item').click(function() {
			var url = siteInfo.baseUrl + 'api/live-editor/page-element/' + $(this).attr('data-id');
			var item = $(this);
			var dataName = item.attr('data-live-editor-name');
			var icon = item.find('.icon');

			icon.removeClass('unhide');
			icon.addClass('loading');

			$.get(url, function(data) {
				icon.addClass('unhide');
				icon.removeClass('loading');

				$('*[data-live-editor-name="' + data.name + '"]').html(data.content);
			}, 'json');

			return false;
		});
	},

	openUploadWindow: function() {
		// If the media frame already exists, reopen it.
		if (Modules.liveEditor.fileFrame) {
			Modules.liveEditor.fileFrame.open();
			return;
		}
		 
		// Create the media frame.
		Modules.liveEditor.fileFrame = wp.media.frames.file_frame = wp.media({
			title: 'Fotos do Wordpress',
			button: {
				text: 'Inserir Foto',
			},
			multiple: false // Set to true to allow multiple files to be selected
		});
		 
		// When an image is selected, run a callback.
		Modules.liveEditor.fileFrame.on('select', function() {
			// We set multiple to false so only get one image from the uploader
			attachment = Modules.liveEditor.fileFrame.state().get('selection').first().toJSON();
			// Do something with attachment.id and/or attachment.url here
			Modules.liveEditor.currentEditor.insertHtml('<img src="' + attachment.url + '" />');
		});
		 
		// Finally, open the modal
		Modules.liveEditor.fileFrame.open();
	}
} 