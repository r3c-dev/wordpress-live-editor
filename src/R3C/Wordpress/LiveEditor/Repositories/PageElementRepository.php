<?php
namespace R3C\Wordpress\LiveEditor\Repositories;

use Doctrine\ORM\EntityRepository;

use R3C\Wordpress\Doctrine\DoctrineFactory;
use R3C\Wordpress\LiveEditor\Entities\PageElement;

class PageElementRepository extends EntityRepository
{
    public static function createFromForm($inputs)
    {
        $pageElement = new PageElement();

        $pageElement->setName($inputs['name']);
        $pageElement->setContent($inputs['content']);

        return $pageElement;
    }

    public function findLastByName($name)
    {
        return $this->findBy([
            'name' => $name
        ], [
            'createdAt' => 'DESC'
        ], 1);
    }

    public function findLast10ByName($name)
    {
        return $this->findBy([
            'name' => $name
        ], [
            'createdAt' => 'DESC'
        ], 10);
    }

    public static function save($pageElement)
    {
        $entityManager = DoctrineFactory::entityManager();

        $entityManager->persist($pageElement);
        $entityManager->flush();

        return true;
    }

    public static function saveFromForm($inputs)
    {
        $pageElement = self::createFromForm($inputs);
        return self::save($pageElement);
    }
}