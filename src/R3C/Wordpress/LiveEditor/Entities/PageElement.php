<?php
namespace R3C\Wordpress\LiveEditor\Entities;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use R3C\Wordpress\Doctrine\Entities\BaseEntity;

/**
* @ORM\Entity(repositoryClass="R3C\Wordpress\LiveEditor\Repositories\PageElementRepository")
* @ORM\Table(name="wp_live_editor_page_element")
* Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
*/
class PageElement extends BaseEntity
{
    /**
     * @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="name")
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="text", name="content")
     * @var string
     */
    protected $content;

    /**
     * Gets the value of id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param string $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets the value of content.
     *
     * @param string $content the content
     *
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }
}