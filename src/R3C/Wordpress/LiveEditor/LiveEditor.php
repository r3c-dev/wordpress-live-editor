<?php
namespace R3C\Wordpress\LiveEditor;

use R3C\Wordpress\Doctrine\DoctrineFactory;
use R3C\Wordpress\LiveEditor\Repositories\PageElementRepository;

class LiveEditor
{
    public static function pageElement($name, $defaultFunction)
    {
        $entityManager = DoctrineFactory::entityManager();

        $result = $entityManager->getRepository('R3C\Wordpress\LiveEditor\Entities\PageElement')
                ->findLastByName($name);

        if (count($result) == 0) {
            //Se não existir no banco, captura a saída e faz a primeira inserção
            ob_start();

            $defaultFunction();

            $defaultContent = ob_get_contents();

            ob_end_clean();

            echo $defaultContent;

            PageElementRepository::saveFromForm([
                'name' => $name,
                'content' => $defaultContent
            ]);
        } else {
            $result = $result[0];

            echo stripcslashes(htmlspecialchars_decode($result->getContent()));
        }
    }
}