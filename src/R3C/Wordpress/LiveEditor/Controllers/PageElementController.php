<?php
namespace R3C\Wordpress\LiveEditor\Controllers;

use DateTime;
use R3C\Wordpress\Router\Controllers\ResourceControllerInterface;
use R3C\Wordpress\LiveEditor\Repositories\PageElementRepository;

use R3C\Wordpress\Doctrine\DoctrineFactory;

class PageElementController implements ResourceControllerInterface
{
	//A index é uma listagem da entidade, com ou sem filtro
    public function index()
    {
    	$entityManager = DoctrineFactory::entityManager();

    	if (isset($_GET['name']) && isset($_GET['last'])) {
    		$result = $entityManager->getRepository('R3C\Wordpress\LiveEditor\Entities\PageElement')
    			->findLastByName($_GET['name']);
    	} else if (isset($_GET['name'])) {
    		$result = $entityManager->getRepository('R3C\Wordpress\LiveEditor\Entities\PageElement')
    			->findLast10ByName($_GET['name']);

            foreach ($result as $item) {
                $item->setContent('');
            }
    	}

    	//echo stripcslashes(htmlspecialchars_decode($metaValue['value']));

    	return json_encode($result);
    }

    //As funções abaixo são utilizadas para mostrar formulários
    public function create()
    {

    }

    public function edit($id)
    {

    }

    //As funções abaixo são utilizadas para respostas via AJAX
    public function destroy($id)
    {

    }

    public function show($id)
    {
		$entityManager = DoctrineFactory::entityManager();

		$result = $entityManager->getRepository('R3C\Wordpress\LiveEditor\Entities\PageElement')
    			->find($id);

    	//echo stripcslashes(htmlspecialchars_decode($metaValue['value']));

        if ($result) {
            $result->setContent(stripcslashes(htmlspecialchars_decode($result->getContent())));
        }

    	return json_encode($result);
    }

    public function store()
    {
        if (isset($_POST['name']) && is_array($_POST['name'])) {
            foreach ($_POST['name'] as $key => $name) {
                $result = PageElementRepository::saveFromForm([
                    'name' => $name,
                    'content' => $_POST['content'][$key]
                ]);
            }

            return json_encode(['success' => true]);
        } else {
        	$result = PageElementRepository::saveFromForm([
        		'name' => $_POST['name'],
        		'content' => $_POST['content']
        	]);

        	if ($result) {
        		return json_encode(['success' => true]);
        	}

        	return json_encode(['success' => false]);
        }
    }

    public function update($id)
    {

    }
}