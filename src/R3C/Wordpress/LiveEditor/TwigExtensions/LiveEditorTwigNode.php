<?php
namespace R3C\Wordpress\LiveEditor\TwigExtensions;

use R3C\Wordpress\LiveEditor\LiveEditor;

use Twig_Node;

class LiveEditorTwigNode extends \Twig_Node
{

	public function __construct($params, $lineno = 0, $tag = null)
	{
		parent::__construct(array ('params' => $params), array (), $lineno, $tag);
	}

	public function compile(\Twig_Compiler $compiler)
	{
		$count = count($this->getNode('params'));

		$compiler
			->addDebugInfo($this);

		for ($i = 0; ($i < $count); $i++)
		{
			// argument is not an expression (such as, a \Twig_Node_Textbody)
			// we should trick with output buffering to get a valid argument to pass
			// to the functionToCall() function.
			if (!($this->getNode('params')->getNode($i) instanceof \Twig_Node_Expression)) {
				$compiler
					->write('ob_start();')
					->raw(PHP_EOL);

				$compiler
					->subcompile($this->getNode('params')->getNode($i));

				$compiler
					->write('$_liveEditor[] = ob_get_clean();')
					->raw(PHP_EOL);
			} else {
				$compiler
					->write('$_liveEditor[] = ')
					->subcompile($this->getNode('params')->getNode($i))
					->raw(';')
					->raw(PHP_EOL);
			}
		}

		$compiler
			->write('$content = $_liveEditor[0];')
			->raw(PHP_EOL);

		$compiler
			->write('array_shift($_liveEditor);')
			->raw(PHP_EOL);

		$compiler
			->write('array_push($_liveEditor, function() use ($content) { echo $content; });')
			->raw(PHP_EOL);


		$compiler
			->write('call_user_func_array(')
			->string('\R3C\Wordpress\LiveEditor\LiveEditor::pageElement')
			->raw(', $_liveEditor);')
			->raw(PHP_EOL);

		$compiler
			->write('unset($_liveEditor);')
			->raw(PHP_EOL);
	}

}
