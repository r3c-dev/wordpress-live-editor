<?php
namespace R3C\Wordpress\LiveEditor\TwigExtensions;

use Twig_Extension;

class LiveEditorTwigExtension extends Twig_Extension
{
	public function getTokenParsers()
	{
		return [
			new LiveEditorTwigTokenParser(),
		];
	}

	public function getName()
	{
		return 'liveEditor';
	}
}